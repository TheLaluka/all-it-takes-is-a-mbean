# All it takes is a Mbean

> At this point, our inspector already knew that the culprit was Sir. Jolokia! But Jolokia was hiding in plain sight. \
That was both ludicrous and infuriating. It is now your mission to help uncover the suspects and take them down.


# Setup

```bash
docker-compose up
# In crontab
*/30 * * * * root bash -c 'cd /all-it-takes-is-a-mbean; docker-compose stop; docker-compose rm -f;docker-compose up'
```


# Test solvability

```bash
# Patch ENV before running! :)
./solve.sh
```
